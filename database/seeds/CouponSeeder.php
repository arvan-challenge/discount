<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CouponSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coupons')->insert([
            'title' => 'Iran & Brazil final',
            'code' => 'OFINALO',
            'type' => 'static',
            'value' => 10000000,
            'max_hits' => 100,
            'expire' => '2020-10-01T22:35:00',
            'status' => 'active',
        ]);
    }
}
