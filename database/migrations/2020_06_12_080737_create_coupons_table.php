<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            
            $table->string('title')->nullable();
            $table->string('code')->unique();
            $table->enum('type', ['static'])->default('static');
            $table->bigInteger('value');
            $table->bigInteger('max_hits');
            $table->bigInteger('hits');
            $table->timestamp('expire');
            $table->enum('status', ['active', 'pending', 'deactive'])->default('pending');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
