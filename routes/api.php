<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('/v1/coupons')->group(function () {
    Route::post('/use', 'DiscountController@useCoupon');

    Route::post('/reserve', 'DiscountController@reserveCoupon');

    Route::put('/usages/reject', 'DiscountController@rejectCouponUsage');

    Route::put('/usages/accept', 'DiscountController@acceptCouponUsage');

    Route::get('/{code}/usages', 'DiscountController@fetchCouponUsages');

    Route::get('/{code}', 'DiscountController@getCouponByCode');

    Route::get('/', 'DiscountController@fetchCoupons');
});
