<?php

namespace Pkg\Pagination;

/**
 * Pagination parameters
 */
class PaginationParams
{
    public int $limit;
    public int $page;

    public function __construct(int $limit, int $page)
    {
        $this->limit = $limit;
        $this->page = $page;
    }
}
