## Intro

Discount Service


## Start Service

```bash
composer install
php artisan serve --port 8000
php artisan rabbitevents:listen discount.couponRejected --memory=512 --timeout=60 --tries=30 --sleep=5
```