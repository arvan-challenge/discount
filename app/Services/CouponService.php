<?php

namespace App\Services;

use App\Contracts\CouponRepository;
use App\Contracts\CouponService as CouponServiceContract;
use App\Coupon;
use App\CouponStatus;
use App\Exceptions\CouponAlreadyUsedException;
use App\Exceptions\CouponIsOver;
use App\Exceptions\ExpiredCouponException;
use App\Exceptions\InvalidCouponException;
use App\UsedCouponStatus;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Pkg\Pagination\PaginationParams;

/**
 * Coupon service implementation
 */
class CouponService implements CouponServiceContract
{
    /**
     * Coupon repository instance
     * @var CouponRepository $repository
     */
    protected CouponRepository $repository;

    /**
     * Contruct a coupon service
     *
     * @param CouponRepository $repository
     */
    public function __construct(CouponRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Is coupon active
     *
     * @param Coupon $coupon
     * @return boolean
     */
    private function _isCouponActive(Coupon $coupon): bool
    {
        return $coupon->status == CouponStatus::ACTIVE;
    }

    /**
     * Is coupon expired
     *
     * @param Coupon $coupon
     * @return boolean
     */
    private function _isCouponExpired(Coupon $coupon): bool
    {
        $now = new Carbon();

        return $coupon->expire < $now;
    }

    /**
     * Create new coupon
     *
     * @param Coupon $coupon
     * @return Coupon|null
     */
    public function createCoupon(Coupon $coupon): ?Coupon
    {
        $coupon->code = Str::random(8);

        $coupon = $this->repository->createCoupon($coupon);

        return $coupon;
    }

    /**
     * Fetch coupons
     *
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchCoupons(PaginationParams $pp) {
        return $this->repository->fetchCoupons($pp);
    }

    /**
     * Use a coupon
     *
     * @param string $code
     * @param string $userID
     * @return void
     */
    public function useCoupon(string $code, string $userID): ?Coupon
    {
        $this->repository->startTransaction();

        try {
            $coupon = $this->repository->getCouponByCodeL($code);

            if (!$coupon || !$this->_isCouponActive($coupon)) {
                throw new InvalidCouponException('coupon is invalid');
            }

            if ($coupon->hits >= $coupon->max_hits) {
                throw new CouponIsOver('coupon is over');
            }

            if ($this->_isCouponExpired($coupon)) {
                throw new ExpiredCouponException('coupon expired');
            }

            if ($this->repository->couponAlreadyUsed($coupon->id, $userID)) {
                throw new CouponAlreadyUsedException('coupon already used');
            }

            $coupon->hits++;
            $this->repository->updateCouponHits($coupon->id, $coupon->hits);

            $this->repository->logCouponUsage($coupon->id, $userID, UsedCouponStatus::ACCEPTED);

            $this->repository->commit();

            return $coupon;
        } catch (\Exception $e) {
            $this->repository->rollBack();

            throw $e;
        }
    }

    /**
     * Reserve a coupon
     *
     * @param string $code
     * @param string $userID
     * @return void
     */
    public function reserveCoupon(string $code, string $userID): ?Coupon
    {
        $this->repository->startTransaction();

        try {
            $coupon = $this->repository->getCouponByCodeL($code);

            if (!$coupon || !$this->_isCouponActive($coupon)) {
                throw new InvalidCouponException('coupon is invalid');
            }

            if ($coupon->hits >= $coupon->max_hits) {
                throw new CouponIsOver('coupon is over');
            }

            if ($this->_isCouponExpired($coupon)) {
                throw new ExpiredCouponException('coupon expired');
            }

            if ($this->repository->couponAlreadyUsed($coupon->id, $userID)) {
                throw new CouponAlreadyUsedException('coupon already used');
            }

            $coupon->hits++;
            $this->repository->updateCouponHits($coupon->id, $coupon->hits);

            $this->repository->logCouponUsage($coupon->id, $userID, UsedCouponStatus::PENDING);

            $this->repository->commit();

            return $coupon;
        } catch (\Exception $e) {
            $this->repository->rollBack();

            throw $e;
        }
    }

    /**
     * Accept a used coupon
     *
     * @param string $couponCode
     * @param string $userID
     * @return bool
     */
    public function acceptUsedCoupon(string $couponCode, string $userID): bool
    {
        $coupon = $this->getCouponByCode($couponCode);

        return $this->repository->updateCouponUsage($coupon->id, $userID, UsedCouponStatus::ACCEPTED);
    }

    /**
     * Reject a used coupon
     *
     * @param string $couponCode
     * @param string $userID
     * @return bool
     */
    public function rejectUsedCoupon(string $couponCode, string $userID): bool
    {
        $this->repository->startTransaction();

        try {
            $coupon = $this->repository->getCouponByCodeL($couponCode);

            if (!$coupon || !$this->_isCouponActive($coupon)) {
                throw new InvalidCouponException('coupon is invalid');
            }

            $this->repository->updateCouponHits($coupon->id, $coupon->hits - 1);

            $this->repository->deleteCouponUsage($coupon->id, $userID);

            $this->repository->commit();

            return true;
        } catch (\Exception $e) {
            $this->repository->rollBack();

            throw $e;
        }
    }

    /**
     * Fetch coupon usages
     *
     * @param string $couponCode
     * @param PaginationParams $pp
     * @return (CouponCode[], PaginationInfo)
     */
    public function fetchCouponUsages(string $couponCode, PaginationParams $pp)
    {
        $coupon = $this->repository->getCouponByCode($couponCode);
        if (!$coupon) {
            throw new ModelNotFoundException('coupon not found');
        }

        return $this->repository->couponUsages($coupon->id, $pp);
    }

    /**
     * Get a coupon by its unique code
     *
     * @param string $code
     * @return Coupon|null
     */
    public function getCouponByCode(string $couponCode): ?Coupon
    {
        $coupon = $this->repository->getCouponByCode($couponCode);
        if (!$coupon) {
            throw new ModelNotFoundException('coupon not found');
        }

        return $coupon;
    }
}
