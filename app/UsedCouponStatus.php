<?php

namespace App;

/**
 * UsedCouponStatus
 */
class UsedCouponStatus
{
    const PENDING = 'pending';
    const REJECTED = 'rejected';
    const ACCEPTED = 'accepted';
}
