<?php

namespace App;

/**
 * CouponStatus
 */
class CouponStatus
{
    const ACTIVE = 'active';
    const PENDING = 'pending';
    const DEACTIVE = 'deactive';
}
