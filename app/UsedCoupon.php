<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsedCoupon extends Model
{
    protected $table = 'used_coupons';

    protected $fillable = [
        'user_id',
        'coupon_id',
        'status',
    ];
}
