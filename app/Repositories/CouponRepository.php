<?php

namespace App\Repositories;

use App\Contracts\CouponRepository as CouponRepositoryContract;
use App\Coupon;
use App\Traits\HandleTransaction;
use App\UsedCoupon;
use App\UsedCouponStatus;
use Pkg\Pagination\Pagination;
use Pkg\Pagination\PaginationParams;

/**
 * Coupon Repository
 */
class CouponRepository implements CouponRepositoryContract
{
    use HandleTransaction;

    /**
     * Create new coupon
     *
     * @param Coupon $coupon
     * @return Coupon|null
     */
    public function createCoupon(Coupon $coupon): ?Coupon
    {
        $coupon->save();

        return $coupon;
    }

    /**
     * get a coupon with id
     *
     * @param int $couponID
     * @return Coupon|null
     */
    public function getCoupon(int $couponID): ?Coupon
    {
        return Coupon::where("id", $couponID)->first();
    }

    /**
     * get a coupon with id and lock writes
     *
     * @param int $couponID
     * @return Coupon|null
     */
    public function getCouponL(int $couponID): ?Coupon
    {
        return Coupon::where("id", $couponID)->lockForUpdate()->first();
    }

    /**
     * Get coupon by code
     *
     * @param string $code
     * @return Coupon|null
     */
    public function getCouponByCode(string $code): ?Coupon
    {
        return Coupon::where("code", $code)->first();
    }

    /**
     * Get coupon by code and lock writes
     *
     * @param string $code
     * @return Coupon|null
     */
    public function getCouponByCodeL(string $code): ?Coupon
    {
        return Coupon::where("code", $code)->lockForUpdate()->first();
    }

    /**
     * Fetch coupons
     *
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchCoupons(PaginationParams $pp)
    {
        $total = Coupon::count();

        $paginationInfo = Pagination::paginate($pp->limit, $pp->page, $total);

        $coupons = Coupon::limit($paginationInfo->limit)
            ->offset($paginationInfo->offset)
            ->get();

        return [$coupons, $paginationInfo];
    }

    /**
     * Fetch coupon usages
     *
     * @param integer $couponID
     * @param PaginationParams $pp
     * @return (CouponUsage[], PaginationInfo)
     */
    public function couponUsages(int $couponID, PaginationParams $pp)
    {
        $total = UsedCoupon::where('coupon_id', $couponID)->count();

        $paginationInfo = Pagination::paginate($pp->limit, $pp->page, $total);

        $usages = UsedCoupon::where('coupon_id', $couponID)
            ->where('status', UsedCouponStatus::ACCEPTED)
            ->limit($paginationInfo->limit)
            ->offset($paginationInfo->offset)
            ->get();

        return [$usages, $paginationInfo];
    }

    /**
     * Determine that a coupon used by a user or not
     *
     * @param int $couponID
     * @param string $userID
     * @return bool
     */
    public function couponAlreadyUsed(int $couponID, string $userID): bool
    {
        $used = UsedCoupon::where("coupon_id", $couponID)
            ->where("user_id", $userID)
            ->first();

        return !is_null($used);
    }

    /**
     * Update a coupon hits count
     *
     * @param integer $couponID
     * @param integer $hits
     * @return boolean
     */
    public function updateCouponHits(int $couponID, int $hits): bool
    {
        return Coupon::where("id", $couponID)->update(["hits" => $hits]);
    }

    /**
     * Log a coupon usage
     *
     * @param integer $couponID
     * @param string $userID
     * @param string $status
     * @return boolean
     */
    public function logCouponUsage(int $couponID, string $userID, string $status = UsedCouponStatus::PENDING): bool
    {
        $log = new UsedCoupon();
        $log->user_id = $userID;
        $log->coupon_id = $couponID;
        $log->status = $status;

        return $log->save();
    }

    /**
     * Update a coupon usage
     *
     * @param string $status
     * @return bool
     */
    public function updateCouponUsage(int $couponID, string $userID, string $status): bool
    {
        return UsedCoupon::where("coupon_id", $couponID)
            ->where("user_id", $userID)
            ->where("status", UsedCouponStatus::PENDING)
            ->update(["status" => $status]);
    }

    /**
     * Delete coupon usage
     *
     * @param string $status
     * @return bool
     */
    public function deleteCouponUsage(int $couponID, string $userID): bool
    {
        return UsedCoupon::where("coupon_id", $couponID)
            ->where("user_id", $userID)
            ->where("status", UsedCouponStatus::PENDING)
            ->delete();
    }
}
