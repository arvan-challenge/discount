<?php

namespace App\Http\Controllers;

use App\Contracts\CouponService;
use App\Exceptions\CouponAlreadyUsedException;
use App\Exceptions\CouponIsOver;
use App\Exceptions\ExpiredCouponException;
use App\Exceptions\InvalidCouponException;
use App\Http\Requests\UseCoupon;
use App\Templates\ResponseTemplate;
use Illuminate\Http\Request;
use Pkg\Pagination\Pagination;

class DiscountController extends Controller
{
    /**
     * @var CouponService coupon service instance
     */
    protected CouponService $couponService;

    public function __construct(CouponService $couponService)
    {
        $this->couponService = $couponService;
    }

    /**
     * Use a coupon
     *
     * @param UseCoupon $req
     * @return void
     */
    public function useCoupon(UseCoupon $req)
    {
        $userID = $req->get('user_id');
        $couponCode = $req->get('coupon_code');

        try {
            $coupon = $this->couponService->useCoupon($couponCode, $userID);

            return ResponseTemplate::Ok([
                'msg' => 'coupon used',
                'coupon' => $coupon,
            ]);
        } catch (InvalidCouponException | ExpiredCouponException $e) {
            return ResponseTemplate::BadRequest($e, 'InvalidCoupon');
        } catch (CouponIsOver $e) {
            return ResponseTemplate::BadRequest($e, 'CouponIsOver');
        } catch (CouponAlreadyUsedException $e) {
            return ResponseTemplate::BadRequest($e, 'CouponAlreadyUsed');
        } catch (\Exception $e) {
            return ResponseTemplate::InternalServerError($e);
        }
    }

    /**
     * Reserve a coupon
     *
     * @param UseCoupon $req
     * @return void
     */
    public function reserveCoupon(UseCoupon $req)
    {
        $userID = $req->get('user_id');
        $couponCode = $req->get('coupon_code');

        try {
            $coupon = $this->couponService->reserveCoupon($couponCode, $userID);

            return ResponseTemplate::Ok([
                'msg' => 'coupon reserved',
                'coupon' => $coupon,
            ]);
        } catch (InvalidCouponException | ExpiredCouponException $e) {
            return ResponseTemplate::BadRequest($e, 'InvalidCoupon');
        } catch (CouponIsOver $e) {
            return ResponseTemplate::BadRequest($e, 'CouponIsOver');
        } catch (CouponAlreadyUsedException $e) {
            return ResponseTemplate::BadRequest($e, 'CouponAlreadyUsed');
        } catch (\Exception $e) {
            return ResponseTemplate::InternalServerError($e);
        }
    }

    /**
     * Accept used coupon
     *
     * @param Request $req
     * @return void
     */
    public function acceptCouponUsage(UseCoupon $req)
    {
        $userID = $req->get('user_id');
        $couponCode = $req->get('coupon_code');

        try {
            $this->couponService->acceptUsedCoupon($couponCode, $userID);

            return ResponseTemplate::Ok([
                'msg' => 'coupon accepted',
            ]);
        } catch (InvalidCouponException $e) {
            return ResponseTemplate::BadRequest($e, 'InvalidCoupon');
        } catch (\Exception $e) {
            return ResponseTemplate::InternalServerError($e);
        }
    }

    /**
     * Reject used coupon
     *
     * @param Request $req
     * @return void
     */
    public function rejectCouponUsage(UseCoupon $req)
    {
        $userID = $req->get('user_id');
        $couponCode = $req->get('coupon_code');

        try {
            $this->couponService->rejectUsedCoupon($couponCode, $userID);

            return ResponseTemplate::Ok([
                'msg' => 'coupon rejected',
            ]);
        } catch (InvalidCouponException $e) {
            return ResponseTemplate::BadRequest($e, 'InvalidCoupon');
        } catch (\Exception $e) {
            return ResponseTemplate::InternalServerError($e);
        }
    }

    /**
     * Fetch coupons
     *
     * @param Request $req
     * @return void
     */
    public function fetchCoupons(Request $req)
    {
        $pp = Pagination::parsePaginationParams($req);

        list($coupons, $paginationInfo) = $this->couponService->fetchCoupons($pp);

        return ResponseTemplate::OkWithPagination([
            'coupons' => $coupons,
        ], $paginationInfo);
    }

    /**
     * Fetch coupon usages
     *
     * @param Request $req
     * @param string $couponCode
     * @return void
     */
    public function fetchCouponUsages(Request $req, string $couponCode)
    {
        $pp = Pagination::parsePaginationParams($req);

        list($usages, $paginationInfo) = $this->couponService->fetchCouponUsages($couponCode, $pp);

        return ResponseTemplate::OkWithPagination([
            'usages' => $usages,
        ], $paginationInfo);
    }

    /**
     * Get a coupon by its unique code
     *
     * @param Request $req
     * @param string $couponCode
     * @return void
     */
    public function getCouponByCode(Request $req, string $couponCode)
    {
        $coupon =  $this->couponService->getCouponByCode($couponCode);

        return ResponseTemplate::Ok([
            'coupon' => $coupon,
        ]);
    }
}
