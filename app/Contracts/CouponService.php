<?php

namespace App\Contracts;

use App\Coupon;
use Pkg\Pagination\PaginationParams;

/**
 * Coupon service
 */
interface CouponService
{
    /**
     * Create new coupon
     *
     * @param Coupon $coupon
     * @return Coupon|null
     */
    public function createCoupon(Coupon $coupon): ?Coupon;

    /**
     * Fetch coupons
     *
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchCoupons(PaginationParams $pp);

    /**
     * Fetch coupon usages
     *
     * @param string $couponCode
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchCouponUsages(string $couponCode, PaginationParams $pp);

    /**
     * Use a coupon
     *
     * @param string $code
     * @param string $userID
     * @return void
     */
    public function useCoupon(string $couponCode, string $userID): ?Coupon;

    /**
     * Reserve a coupon
     *
     * @param string $code
     * @param string $userID
     * @return void
     */
    public function reserveCoupon(string $couponCode, string $userID): ?Coupon;

    /**
     * Accept a used coupon
     *
     * @param string $code
     * @param string $userID
     * @return void
     */
    public function acceptUsedCoupon(string $couponCode, string $userID): bool;

    /**
     * Reject a used coupon
     *
     * @param string $code
     * @param string $userID
     * @return void
     */
    public function rejectUsedCoupon(string $couponCode, string $userID): bool;

    /**
     * Get a coupon by its unique code
     *
     * @param string $code
     * @return Coupon|null
     */
    public function getCouponByCode(string $couponCode): ?Coupon;
}
