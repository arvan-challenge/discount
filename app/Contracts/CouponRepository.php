<?php

namespace App\Contracts;

use App\Coupon;
use App\UsedCouponStatus;
use Pkg\Pagination\PaginationParams;

/**
 * Coupon repository
 */
interface CouponRepository extends TransactionalRepository
{
    /**
     * Create new coupon
     *
     * @param Coupon $coupon
     * @return Coupon|null
     */
    public function createCoupon(Coupon $coupon): ?Coupon;

    /**
     * get a coupon with id
     *
     * @param int $couponID
     * @return Coupon|null
     */
    public function getCoupon(int $couponID): ?Coupon;

    /**
     * get a coupon with id and lock writes
     *
     * @param int $couponID
     * @return Coupon|null
     */
    public function getCouponL(int $couponID): ?Coupon;

    /**
     * Get coupon by code
     *
     * @param string $code
     * @return Coupon|null
     */
    public function getCouponByCode(string $code): ?Coupon;

    /**
     * Get coupon by code and lock writes
     *
     * @param string $code
     * @return Coupon|null
     */
    public function getCouponByCodeL(string $code): ?Coupon;

    /**
     * Fetch coupons
     *
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchCoupons(PaginationParams $pp);

    /**
     * Fetch coupon usages
     *
     * @param integer $couponID
     * @param PaginationParams $pp
     * @return (CouponUsage[], PaginationInfo)
     */
    public function couponUsages(int $couponID, PaginationParams $pp);

    /**
     * Determine that a coupon used by a user or not
     *
     * @param int $couponID
     * @param string $userID
     * @return boolean
     */
    public function couponAlreadyUsed(int $couponID, string $userID): bool;

    /**
     * Update a coupon hits count
     *
     * @param integer $couponID
     * @param integer $hits
     * @return boolean
     */
    public function updateCouponHits(int $couponID, int $hits): bool;

    /**
     * Log a coupon usage
     *
     * @param integer $couponID
     * @param string $userID
     * @param string $status
     * @return boolean
     */
    public function logCouponUsage(int $couponID, string $userID, string $status = UsedCouponStatus::PENDING): bool;

    /**
     * Update a coupon usage
     *
     * @param string $status
     * @return bool
     */
    public function updateCouponUsage(int $couponID, string $userID, string $status): bool;

    /**
     * Delete coupon usage
     *
     * @param string $status
     * @return bool
     */
    public function deleteCouponUsage(int $couponID, string $userID): bool;
}
