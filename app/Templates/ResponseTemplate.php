<?php

namespace App\Templates;

use Illuminate\Http\JsonResponse;
use Pkg\Pagination\PaginationInfo;

/**
 * Base Response
 */
class BaseResponse
{
    public $status;
    public $code;
    public $data;
    public $meta;

    public function __construct($data, $code = 'OK', $status = 200, $meta = null)
    {
        $this->data = $data;
        $this->code = $code;
        $this->status = $status;
        $this->meta = $meta;
    }
}

/**
 * Response template generation helper
 */
class ResponseTemplate
{
    protected static function _parseErr($err): string
    {
        if ($err instanceof \Exception) {
            return $err->getMessage();
        }

        return strval($err);
    }

    /**
     * Ok template
     *
     * @param any $data
     * @param string $code
     * @return JsonResponse
     */
    public static function Ok($data, string $code = 'Ok'): JsonResponse
    {
        return response()->json(
            new BaseResponse($data, $code, 200, null),
            200
        );
    }

    /**
     * Ok with pagination info template
     *
     * @param any $data
     * @param string $code
     * @return JsonResponse
     */
    public static function OkWithPagination($data, PaginationInfo $pi, string $code = 'Ok'): JsonResponse
    {
        return response()->json(
            new BaseResponse($data, $code, 200, [
                'total' => $pi->total,
                'limit' => $pi->limit,
                'page' => $pi->page,
                'pages' => $pi->pages,
                'offset' => $pi->offset,
                'next' => $pi->next,
                'prev' => $pi->prev,
            ]),
            200
        );
    }

    /**
     * Internal server error template
     *
     * @param any $err
     * @param string $code
     * @return JsonResponse
     */
    public static function InternalServerError($err, string $code = 'InternalServerError'): JsonResponse
    {
        return response()->json(new BaseResponse(
            [
                'error' => self::_parseErr($err),
            ],
            $code,
            500,
            null
        ), 500);
    }

    /**
     * bad request response template
     *
     * @param any $err
     * @param string $code
     * @return JsonResponse
     */
    public static function BadRequest($err, string $code): JsonResponse
    {
        return response()->json(new BaseResponse(
            [
                'error' => self::_parseErr($err),
            ],
            $code,
            400,
            null
        ), 400);
    }

    /**
     * Resource not found response template
     *
     * @param any $err
     * @param string $code
     * @return JsonResponse
     */
    public static function NotFound($err, string $code = 'NotFound'): JsonResponse
    {
        return response()->json(new BaseResponse(
            [
                'error' => self::_parseErr($err),
            ],
            $code,
            404,
            null
        ), 404);
    }
}
