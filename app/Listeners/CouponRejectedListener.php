<?php

namespace App\Listeners;

use App\Contracts\CouponService;

class CouponRejectedListener
{
    /**
     * @var CouponService coupon service instance
     */
    protected CouponService $couponService;

    public function __construct(CouponService $couponService)
    {
        $this->couponService = $couponService;
    }

    /**
     * Handle the event.
     *
     * @param  array  $event
     * @return void
     */
    public function handle($event)
    {
        $couponCode = $event['code'];
        $userID = $event['user_id'];

        $this->couponService->rejectUsedCoupon($couponCode, $userID);
    }
}
