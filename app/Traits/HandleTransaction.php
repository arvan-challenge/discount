<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

/**
 * Provide database transaction methods
 */
trait HandleTransaction {
    /**
     * Start a transaction
     *
     * @return void
     */
    public function startTransaction(){
        DB::beginTransaction();
    }

    /**
     * RollBack current transaction
     *
     * @return void
     */
    public function rollBack(){
        DB::rollBack();
    }

    /**
     * Commit current transaction
     *
     * @return void
     */
    public function commit() {
        DB::commit();
    }
}