<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CouponServiceProvider extends ServiceProvider
{

    public $bindings = [
        \App\Contracts\CouponRepository::class => \App\Repositories\CouponRepository::class,
        \App\Contracts\CouponService::class => \App\Services\CouponService::class,
    ];

    /**`
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
