<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Coupon
 */
class Coupon extends Model
{
    protected $table = "coupons";

    protected $attributes = [
        "type" => CouponTypes::STATIC,
    ];

    protected $fillable = [
        "title",
        "code",
        "type",
        "value",
        "max_hits",
        "hits",
        "expire",
        "status",        
    ];
}
